const router = require('express').Router()
const articleController = require('../controller/articleController')

router.get('/', articleController.index);

router.post('/create', articleController.create);
router.get('/create', articleController.createArticle)

router.get('/detail/:id', articleController.show);

router.patch('/update/:id', articleController.update);
router.delete('/delete/:id', articleController.delete);

router.get('/:id', articleController.detail);


module.exports = router